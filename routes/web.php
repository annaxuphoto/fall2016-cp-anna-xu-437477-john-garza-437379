<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@home');

Route::get('matchmaker', 'ProfileCardsController@index');

Route::get('profile/{profile}', 'ProfileCardsController@show');

Route::get('register', function (){
    return view('register');
});

Route::post('register/new', 'ProfileCardsController@store');

Route::get('profile/{profile}/edit', 'ProfileCardsController@edit');

Route::patch('profile/{profile}/edit', 'ProfileCardsController@update');
