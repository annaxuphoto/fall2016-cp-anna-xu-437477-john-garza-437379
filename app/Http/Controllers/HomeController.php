<?php
/**
 * Created by PhpStorm.
 * User: john
 * Date: 12/3/16
 * Time: 2:58 PM
 */

namespace App\Http\Controllers;


class HomeController extends Controller
{
    public function home() {
        return view('home');
    }
}