<?php

namespace App\Http\Controllers;

use App\profileCard;
use Illuminate\Http\Request;

class ProfileCardsController extends Controller
{
    public function index() {

        $cards = profileCard::all();

        return view('profileCards.index', compact('cards'));
    }

    public function show(profileCard $profile) {

        return $profile;
    }

    public function store(Request $request, profileCard $saver){

        $card = new profileCard();

        $card->name = $request->name;
        $card->email = $request->email;
        $card->password = $request->password;

        $card->save();

        //return back();
        return redirect('matchmaker');
    }

    public function edit(profileCard $profile){
        return view('profileCards.edit', compact('profile'));

    }

    public function update(Request $request, profileCard $profile){

        $this->validate($request, [
            'description' => 'required'
        ]);

        $profile->location = $request->location;
        $profile->gender = $request->gender;
        $profile->sexuality = $request-> sexuality;
        $profile->age = $request->age;
        $profile->description = $request->description;
        //dd("update");
        //dd($request->all());
        //dd($saver);
        //dd($profile);
        //$profile->update($request->all());

        $profile->update();
        //return redirect('matchmaker');
        //dd($profile);

        //return $profile;

    //    return redirect('profile/2');
    }
}
