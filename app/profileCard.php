<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profileCard extends Model
{

    //protected $primaryKey = 'id';


    //apparently allows everything to be mass assigned? from smart ta, fix this later maybe
    protected $fillable = ['*'];

    //the following 2 functions override the default behavior of storing timestamps
    //but then later I ended up being forced to use it anyway so now they're commented out
    public function setUpdatedAt($value)
    {
        //return parent::setUpdatedAt($value);
    }
    public function setCreatedAt($value)
    {
        //return parent::setCreatedAt($value);
    }
}
