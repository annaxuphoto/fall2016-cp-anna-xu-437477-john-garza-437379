@extends('layout')

@section('content')

    <form method="POST" action="/profile/{{$profile -> id}}/edit">
        {{method_field('PATCH')}}
        {{csrf_field()}}

        <label>Location: </label><br>
        <input type="radio" name="location" value="South 40" checked> South 40<br>
        <input type="radio" name="location" value="Village"> Village<br>
        <input type="radio" name="location" value="Lofts"> Lofts<br>
        <input type="radio" name="location" value="Off Campus"> Off Campus<br><br>

        <label>Gender: </label><br>
        <input type="radio" name="gender" value="Male" checked> Male<br>
        <input type="radio" name="gender" value="Female"> Female<br>
        <input type="radio" name="gender" value="Other"> Other<br><br>

        <label>Preference: </label><br>
        <input type="radio" name="sexuality" value="M" checked> Men<br>
        <input type="radio" name="sexuality" value="F"> Women<br>
        <input type="radio" name="sexuality" value="M/F"> Men & Women<br>
        <input type="radio" name="sexuality" value="O"> Other<br>
        <input type="radio" name="sexuality" value="A"> All<br>
        <input type="radio" name="sexuality" value="F/O"> Women & Other<br>
        <input type="radio" name="sexuality" value="M/O"> Men & Other<br><br>

        <label>Age: </label>
        <input type="number" name="age" value="18"><br><br>

        <label>Description: </label>
        <textarea name="description">Describe yourself!</textarea><br><br>

        <button type="submit">Update Profile!</button>
    </form>

    @if (count($errors))
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    @endif

@stop