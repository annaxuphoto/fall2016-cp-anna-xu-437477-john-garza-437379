@extends('layout')

@section('content')
    <form method="POST" action="/register/new">
        {{csrf_field()}}

        <label>Name: </label> <input type="text" name="name"> <br>
        <label>wustl.edu email : </label> <input type="text" name="email"> <br>
        <label>Password: </label> <input type="password" name="password"> <br>
        <label>Retype password: </label> <input type="password" name="checkpw"> <br>
        <button type="submit">Register!</button>
    </form>
@stop