<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profileCards', function (Blueprint $table) {

            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();

            $table->string('name');
            $table->string('residence');
            $table->string('picture_placeholder');
            $table->integer('age');
            $table->string('description');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profileCards');
    }
}
