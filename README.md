CSE330S CREATIVE PROJECT RUBRIC: Anna Xu, John Garza

Rubric: 5 points

Project Idea: Wash U Dating Website (75 points)  
User Database: 5 points  
    Passwords  
    Name  
    Location  
    Email  
    Photo  
    Gender  
    Sexuality  
    Description  
    Age  
Match Database: 5 points  
    Who likes whom  
    Who dislikes whom  
Filter by location (South 40, Village, Lofts, etc.), preferences: 5 points  
Email Validation: 5 points  
    only @wustl.edu emails  
User Profile: 10 points  
    See all matches  
    Update information  
Main Page: 15 points  
    Upon login, users see a scroll view of potential matches  
    Can like or dislike profiles  
    Actioned profiles disappear from view  
Unmatch Feature: 5 points  
Privacy: 5 points  
    Site only available to registered users  
    Will not display until logged in  
Bootstrap: 7.5 points  
    Site is aesthetically pleasing and easy to use
Laravel: 7.5 points
    Laravel is set up properly and used significantly throughout the project
Security: 5 points  
    Filter input, escape output, encrypt passwords, tokens  

New Frameworks: PHP Laravel, Bootstrap (CSS)


Creative: 20 points
    Push likes to top of stack
    Email for each new match
    Undo feature